import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, NavController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LecturePage } from "../pages/lecture/lecture";
import { DownloadedPage } from "../pages/downloaded/downloaded";
import { AboutusPage } from "../pages/aboutus/aboutus";
import { SettingsPage } from "../pages/settings/settings";
import { LivePage } from "../pages/live/live";
import { CardPage } from "../pages/card/card";
import { MemCardsPage } from "../pages/memcards/memcards";
import { CurrentPage } from "../pages/current/current";
import { myaccountPage } from "../pages/myaccount/myaccount";
import { faqPage } from "../pages/faq/faq";
import { termsPage } from "../pages/terms/terms";
import { RecieptPage } from "../pages/reciept/reciept";
import { SignupPage } from "../pages/signup/signup";
import { NativeStorage } from "@ionic-native/native-storage";



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  userData: any;
 // userData: any;

  rootPage:any = HomePage;
  Aboutus = AboutusPage; 
  downloaded = DownloadedPage; 
  HomePage = HomePage;
  lecture = LecturePage;
  settings = SettingsPage;
  live = LivePage;
  card = CardPage;
  list = ListPage;
  memcards = MemCardsPage;
  current = CurrentPage;
  myaccount = myaccountPage;
  faq = faqPage;
  terms = termsPage;
  reciept =RecieptPage;
  signup = SignupPage;
  
  
  

  @ViewChild('nav') nav: NavController;


  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private menuCtrl: MenuController, private storage: Storage,private nativeStorage: NativeStorage) {
    this.initializeApp();

    platform.ready().then(() => {
      // Here we will check if the user is already logged in
      // because we don't want to ask users to log in each time they open the app
      let env = this;
      this.nativeStorage.getItem('user')
      .then( function (data) {
        // user is previously logged and we have his data
        // we will let him access the app
        env.nav.push(LecturePage);
        env.splashScreen.hide();
      }, function (error) {
        //we don't have the user data so we will ask him to log in
        env.nav.push(HomePage);
        env.splashScreen.hide();
      });

      this.statusBar.styleDefault();
    });
     this.getUserdata();



    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'List', component: ListPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  OnLoad(page: any)
  {
    this.nav.setRoot(page);
    this.menuCtrl.close();
  }
    getUserdata(){
     this.storage.get('userdata').then((val) => {
       this.userData= val;
        console.log('userdata is:', val);
      });
  }

}
