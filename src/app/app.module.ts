import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import {HttpModule} from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
//import { OAuthModule } from '../pages/oauth/oauth.module'; //Added for google Auth
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LecturePage } from "../pages/lecture/lecture";
import { SignupPage } from "../pages/signup/signup";
import { AboutusPage } from "../pages/aboutus/aboutus";
import { DownloadedPage } from "../pages/downloaded/downloaded";
import { SettingsPage } from "../pages/settings/settings";
import { LivePage } from "../pages/live/live";
import { CardPage } from "../pages/card/card";
import { HttpProvider } from '../providers/http/http';
import { GooglePlus } from "@ionic-native/google-plus";
import { NativeStorage } from "@ionic-native/native-storage";
import { MemCardsPage } from "../pages/memcards/memcards";
import { CurrentPage } from "../pages/current/current";
import { myaccountPage } from "../pages/myaccount/myaccount";
import { termsPage } from "../pages/terms/terms";
import { faqPage } from "../pages/faq/faq";
import { RecieptPage } from "../pages/reciept/reciept";


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LecturePage,
    SignupPage,
    AboutusPage,
    DownloadedPage,
    SettingsPage,
    LivePage,
    CardPage,
    ListPage,
    CurrentPage,
    myaccountPage,
    termsPage,
    faqPage,
    RecieptPage,
    MemCardsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LecturePage,
    SignupPage,
    AboutusPage,
    DownloadedPage,
    SettingsPage,
    LivePage,
    CardPage,
    ListPage,
    CurrentPage,
    myaccountPage,
    termsPage,
    faqPage,
    RecieptPage,
    MemCardsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Storage,
    HttpProvider,
    GooglePlus,
    NativeStorage
    
  ]
})
export class AppModule {}
