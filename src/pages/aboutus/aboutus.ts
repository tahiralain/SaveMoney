import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { MemCardsPage } from "../memcards/memcards";

@Component({
  selector: 'page-aboutus',
  templateUrl: 'aboutus.html'
})
export class AboutusPage {
  

  text: any;
  myDate: String = new Date().toISOString();
  myDate1: String = new Date().toISOString();
  public number;
  public title;
  public description;
  constructor(public navCtrl: NavController, private menuCtrl: MenuController) {
 // console.log(this.myDate);
 
}
OnOpenMenu()
  {
    this.menuCtrl.open();
  }
   
  public event = {
    //month:this.myDate,
    month:new Date().toISOString(),
    timeEnds: new Date().toISOString()
  }
 move()
 {
  var dropdt = new Date(this.event.month);
  var pickdt = new Date(this.event.timeEnds);
  var timeDiff = Math.abs(pickdt.getTime() - dropdt.getTime());
  var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
 
  // Pushing to next page
  this.navCtrl.push(MemCardsPage,{diffDays,
  param1:this.number,
  param2:this.event.month,
  param3: this.event.timeEnds,
  param4 : this.title,
  param5 : this.description,
  
 });     
 }

 
 
}
