import { DomSanitizer } from '@angular/platform-browser';
import { Component } from '@angular/core';
import { NavController, MenuController, NavParams, LoadingController, Platform } from 'ionic-angular';
import { AboutusPage } from "../aboutus/aboutus";
import { SettingsPage } from "../settings/settings";
import { myaccountPage } from "../myaccount/myaccount";
import { LecturePage } from "../lecture/lecture";
import { ListPage } from "../list/list";

@Component({
  selector: 'page-card',
  templateUrl: 'card.html'
})
export class CardPage {


  constructor(public navCtrl: NavController, public navParams: NavParams, private sanitizer: DomSanitizer, private loadingCtrl: LoadingController, private menuCtrl: MenuController, platform: Platform) {

  }
OnOpenMenu()
  {
    this.menuCtrl.open();
  }
  nav1(){
    this.navCtrl.push(LecturePage);
  }

  nav2(){
    this.navCtrl.push(AboutusPage);
  }

  nav3(){
    this.navCtrl.push(ListPage);
  }

  nav4(){
    this.navCtrl.push(myaccountPage);
  }

  nav5(){
    this.navCtrl.push(SettingsPage);
  }
  
}