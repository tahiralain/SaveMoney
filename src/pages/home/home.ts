import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { LecturePage } from "../lecture/lecture";
import { SignupPage } from "../signup/signup";
import { GooglePlus } from '@ionic-native/google-plus';
import { NativeStorage } from '@ionic-native/native-storage';
import { CardPage } from "../card/card";
import { Http } from "@angular/http";
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
   
    username: '';
    password: '';
    public apiUrl = '';
    public loginDetails='';

   posts:any;
   user: any;
	 userReady: boolean = false;

  splash =  true;
  constructor(public navCtrl: NavController,  private loadingCtrl: LoadingController, private nativeStorage: NativeStorage, public alertCtrl: AlertController,public http: Http, private storage:Storage) {

  }

  homeScreen()
  {
    this.navCtrl.push(CardPage);
  }

  signupScreen()
  {
     this.navCtrl.push(SignupPage);
  }
  ionViewDidLoad() {
  //  this.tabBarElement.style.display = 'none';  (If not using Tab menu)
    setTimeout(() => {
      this.splash = false;
  //    this.tabBarElement.style.display = 'flex';  (If not using Tab menu)
    }, 3300);
  }
 
 // sign-in function start:

 signIn() {

 this.apiUrl = 'https://btckeycard.com/savemoney/login.php?email='+ this.username +'&password=' + this.password;

     if (this.username === '' || this.password === '') {
       let alert = this.alertCtrl.create({
         title: 'Signin Error',
         subTitle: 'Email and Password Required',
         buttons: ['OK']
       });
       alert.present();
       return;
     }


     let loader = this.loadingCtrl.create({
       content: "Signing In..."
     });
     loader.present();

     console.log(this.apiUrl);
  this.http.get(this.apiUrl).map(res => res.json()).subscribe(data => {
   
        console.log(data);
        this.loginDetails=data;
        console.log("login details",this.loginDetails)
           this.userData();
         var str = data;
        var statusMessage = str.Status;
        if(statusMessage === 'success'){
     this.navCtrl.push(CardPage)
         }  

        let alert = this.alertCtrl.create({
           title: 'Sign in Response',
           subTitle: statusMessage,
           buttons: ['OK']
         });
         alert.present();

        loader.dismissAll();
    
     }, error => {
         console.log(error);// Error getting the data

         let alert = this.alertCtrl.create({
           title: 'Sign in Response',
           subTitle: error.toString(),
           buttons: ['OK']
          
         });
        
         alert.present();

         loader.dismissAll();
       });
   
//  this.saveUser();
 }
 // sign-in function end

 userData(){
 this.storage.set('userdata', this.loginDetails);
  }

}
