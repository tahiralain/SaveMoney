import { Component } from '@angular/core';
import { NavController, MenuController, ToastController } from 'ionic-angular';
import { LecturePage } from "../lecture/lecture";
import { HomePage } from "../home/home";

@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html'
})
export class faqPage {

  constructor(public navCtrl: NavController, private menuCtrl: MenuController, public toastCtrl: ToastController) {

  }
  log()
  {
    this.navCtrl.push(HomePage);
  }
  OnOpenMenu()
  {
    this.menuCtrl.open();
  }
   showToastWithCloseButton() {
    const toast = this.toastCtrl.create({
      message: 'Content goes here,Content goes here,Content goes here,Content goes here,Content goes here,Content goes here,Content goes here,Content goes here.',
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present();
  }
}
