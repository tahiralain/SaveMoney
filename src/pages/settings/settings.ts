import { Component } from '@angular/core';
import { NavController, MenuController, ToastController } from 'ionic-angular';
import { LecturePage } from "../lecture/lecture";
import { HomePage } from "../home/home";
import { CardPage } from "../card/card";

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  constructor(public navCtrl: NavController, private menuCtrl: MenuController, public toastCtrl: ToastController) {

  }
  log()
  {
    this.navCtrl.push(HomePage);
  }
  OnOpenMenu()
  {
    this.menuCtrl.open();
  }
logt(){
   let toast = this.toastCtrl.create({
      message: 'Profile Updated',
      duration: 1000,
      position: 'middle'
    });

    toast.present(toast);
  this.navCtrl.push(CardPage);
}
}
