import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, AlertController } from 'ionic-angular';
import { LivePage } from "../live/live";

@Component({
  selector: 'page-current',
  templateUrl: 'current.html'
})
export class CurrentPage {
 

  constructor(public navCtrl: NavController, public menuCtrl: MenuController, public alertCtrl: AlertController) {
    // If we navigated to this page, we will have an item available as a nav param
  }
  OnOpenMenu()
  {
    this.menuCtrl.open();
  }
transaction(){
    this.navCtrl.push(LivePage);
}
  with()
  {
    let confirm = this.alertCtrl.create({
      title: 'Are you sure ?',
      message: '5% would be deducted in order to withdraw before End Date.',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

   within()
  {
    let confirm = this.alertCtrl.create({
      title: 'Are you sure ?',
      message: '',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }
      
  
}
