import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, MenuController } from 'ionic-angular';
import { LecturePage } from "../lecture/lecture";
import { CardPage } from "../card/card";
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {

   username: String = '';
   name: String = '';
   email: String = '';
   country: String = '';
   password: String = '';
   conpassword: String = '';
   private apiUrl = '';
   private storeEmail: any;
   private storePassword: any;
   private storeUsername: any;
   private _addApiUrl = '';
   public address = '';

  constructor(public navCtrl: NavController, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private http: Http,private menu: MenuController,public storage: Storage) {
   this.menu.swipeEnable(false, 'signup');
  }
  homeScreen()
  {
    this.navCtrl.push(LecturePage);
  }
nexto(){
  this.navCtrl.push(CardPage);
}

//sign-up function
 signUp() {

    if (this.username === '' || this.name === '' ||  this.email === '' || this.country === '' || this.password === '' || this.conpassword === '') {
      let alert = this.alertCtrl.create({
        title: 'Signup Error',
        subTitle: 'All fields are rquired',
        buttons: ['OK']
      });
      alert.present();
      return;
    }

    if (this.password !== this.conpassword) {
      let alert = this.alertCtrl.create({
        title: 'Signup Error',
        subTitle: 'Passwords do not match!',
        buttons: ['OK']
      });
      alert.present();
      return;
    }

    this.apiUrl = 'https://btckeycard.com/savemoney/register.php?username=' + this.username + '&name='+ this.name +  '&email=' + this.email + '&country=' + this.country + '&password=' + this.password ;
    let loader = this.loadingCtrl.create({
    content: "Signing Up..."
    });
    loader.present();

    var data = { username: this.username, name: this.name, email: this.email, country: this.country, password: this.password };
    console.log(data);

    this.http.get(this.apiUrl).map(res => res.json())
      .subscribe(data => {

        console.log(data);
        loader.dismiss();
        this.navCtrl.push(CardPage);
        // var jsonData = JSON.parse(data); This used when data coming in string form
        // console.log(jsonData);
        var status = data.Status;

        if (status === 'User Already Exists') {
          
        let alert = this.alertCtrl.create({
          title: 'Signup Response',
          subTitle:'User Already Exist',
          buttons: ['OK']
        });
        alert.present();
          this.navCtrl.push(CardPage);
        }
        else {
         // this.initWallet();
        }

        //redirecting to signin screen after success
      }, error => {
        console.log(error);// Error getting the data

        let alert = this.alertCtrl.create({
          title: 'Signup Response',
          subTitle: data.toString(),
          buttons: ['OK']
        });
        alert.present();

        loader.dismiss();

      });
  }
   setData(){
      this.storage.set('name', this.name);
      this.storage.set('email', this.email);
      this.storage.set('password', this.password);
      this.storage.set('country',this.country);
    }

    getData(){
// Getting email, password and address from storage
    this.storage.get('name').then((val) => {
      console.log('Name is:', val);
    });

      this.storage.get('email').then((val) => {
      console.log('Email is:', val);
    });

    this.storage.get('password').then((val) => {
      console.log('Password is:', val);
    });

    this.storage.get('country').then((val) => {
      console.log('country is:', val);
    });
    }
}
