import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, AlertController, LoadingController } from 'ionic-angular';
import { LivePage } from "../live/live";
import { Http } from '@angular/http';

@Component({
  selector: 'page-myaccount',
  templateUrl: 'myaccount.html'
})
export class myaccountPage {
  daily: string;
  curr_dep_amount: string;
  target_plan_id: string;
  description: string;
  posts: any;
  title: '';
  start_time: '';
  end_time: '';
  number: '';
  model = [];
  
  data:any;
  private apiUrl='https://btckeycard.com/savemoney/fetchtransaction.php';

  constructor(public navCtrl: NavController, public menuCtrl: MenuController, public alertCtrl: AlertController, private http: Http, private loadingCtrl: LoadingController) {
    // If we navigated to this page, we will have an item available as a nav param
  
    this.http.get(this.apiUrl).map(res => res.json()).subscribe(data => {
    console.log(data);
    this.posts = data;
   // console.log(this.posts);
   
    
      }, error => {
         console.log(error);// Error getting the data
      });

  //console.log(this.model);
  }


  OnOpenMenu()
  {
    this.menuCtrl.open();
  }
transaction(){
    this.navCtrl.push(LivePage);
}


 
 delete()
  {
    let confirm = this.alertCtrl.create({
      title: 'Are you sure ?',
      message: 'This will delete and discontinue the current target and you will be charged 5% of the saved amount!',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }






with()
  {
    let confirm = this.alertCtrl.create({
      title: 'Are you sure ?',
      message: '5% would be deducted in order to withdraw before End Date.',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

   within()
  {
    let confirm = this.alertCtrl.create({
      title: 'Are you sure ?',
      message: '',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }
      
  
}
