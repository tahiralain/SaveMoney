import { Component } from '@angular/core';
import { NavController, MenuController, AlertController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { RecieptPage } from "../reciept/reciept";
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { CardPage } from "../card/card";

@Component({
  selector: 'page-memcards',
  templateUrl: 'memcards.html'
})
export class MemCardsPage {
  parameter1: any;
  parameter4: any;
  parameter5: any;
  parameter3:any;
  parameter6: any;
  parameter2 : any;
  start_time : any;
  end_time : any;
  number : any;
  target_plan_id : any;
  amount_dep : any;
  per_dep_amount: any;
  curr_dep_amount:any;
  //daily:any;
  weekly:any;
  week:any;
  month:any;
  monthly:any;
  title:any;
  description: any;
  private apiUrl = '';
  private storeEmail: any;
  private storePassword: any;
  private storeUsername: any;
  private _addApiUrl = '';
  public address = '';

   daily = Math.round(this.number/this.parameter1);
    //this.daily.title
  

  constructor(public navCtrl: NavController, private menuCtrl: MenuController, private alertCtrl: AlertController,private navParams: NavParams, private loadingCtrl: LoadingController, private http: Http,public storage: Storage, public toastCtrl: ToastController) {
    
   
    this.parameter1 = navParams.get('diffDays');
    this.number = navParams.get('param1');
    
    this.title = navParams.get('param4');
    this.description = navParams.get('param5');
    console.log(this.start_time);
    console.log(this.end_time);
    
    // for daily estimation:
    this.daily= Math.round(this.number/this.parameter1);
    //this.daily.title
    console.log(this.daily);

    // for weekly:
    this.week= Math.round(this.parameter1/7);
    console.log(this.week);
    this.weekly = Math.round(this.number/this.week);
    console.log(this.weekly);

    // for monthly:
    this.month= Math.round(this.parameter1/30);
    console.log(this.month);
    this.monthly = Math.round(this.number/this.month);
    console.log(this.monthly);

    //this.title = this.parameter4;
    this.title = this.title;
    console.log(this.title);

    this.description = this.description;
    console.log(this.description);
    
  }

   daycard(){
     var target_plan_id;
     target_plan_id = '1';
     return target_plan_id;
   }
   
OnOpenMenu()
  {
    this.menuCtrl.open();
  }
  myFunction() {
    var x = document.getElementById('myDIV');
    if (x.style.display === 'none') {
        x.style.display = 'block';
    } else {
        x.style.display = 'none';
    }
}
  

  showhide() {       
    var div = document.getElementById("newpost");
    if (div.style.display !== "none") {
        div.style.display = "none";
    }
    else {
        div.style.display = "block";
    }
     }
  showConfirm() {
    let confirm = this.alertCtrl.create({
      title: 'Confirmation',
      message: 'Are you sure you want to use this plan?',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree clicked');
            
          }
        },
        {
          text: 'Agree',
          handler: () => {
            
            console.log('Agree clicked');
             this.storedata();
             this.doAlert();
             this.navCtrl.push(CardPage);
          }
        }
      ]
    });
    confirm.present(); 
  }

  gotoreciept(){
    this.navCtrl.push(RecieptPage);
  }


  storedata() {
    this.curr_dep_amount = '0';
    this.target_plan_id = '1';
    this.start_time = this.navParams.get('param2');
    this.end_time = this.navParams.get('param3');

    if (this.title === '' || this.start_time === '' ||  this.end_time === '' || this.number === '' || this.description === '' || this. target_plan_id === '' || this.curr_dep_amount === '' || this.per_dep_amount === '') {
      let alert = this.alertCtrl.create({
        title: 'Data Sync Error',
        subTitle: 'All fields are required',
        buttons: ['OK']
      });
      alert.present();
      return;
    }

    var data = { title: this.title, start_time: this.start_time, end_time: this.end_time, number: this.number, description: this.description ,target_plan_id: this.target_plan_id ,curr_dep_amount: this.curr_dep_amount , per_dep_amount: this.daily};
    console.log(data);
    this.apiUrl = 'https://btckeycard.com/savemoney/transaction.php?title=' + this.title + '&start_time='+ this.start_time +  '&end_time=' + this.end_time + '&number=' + this.number + '&description=' + this.description +  '&target_plan_id=' + this.target_plan_id +  '&curr_dep_amount=' + this.curr_dep_amount +  '&per_dep_amount=' + this.daily;
    console.log(this.apiUrl);
    let loader = this.loadingCtrl.create({
    content: "Creating Target..."
    });
    loader.present();
    
   

    this.http.get(this.apiUrl).map(res => res.json())
      .subscribe(data => {

        console.log(data);
        loader.dismiss();
        // var jsonData = JSON.parse(data); This used when data coming in string form
        // console.log(jsonData);
        var status = data.Status;
        //redirecting to signin screen after success
      }, error => {
        console.log(error);// Error getting the data

        let alert = this.alertCtrl.create({
          title: 'Signup Response',
          subTitle: data.toString(),
          buttons: ['OK']
        });
        alert.present();
        loader.dismiss();
        
      });
      
      //  this.apiUrl = 'https://btckeycard.com/savemoney/money.php?per_dep_amount='+ this.daily;
      //  console.log(this.apiUrl);
      //  loader.present();
      //  this.http.get(this.apiUrl).map(res => res.json())
      //  .subscribe(data => {

      //   console.log(data);
      //   loader.dismiss();
      //   // var jsonData = JSON.parse(data); This used when data coming in string form
      //   // console.log(jsonData);
      //   var status = data.Status;
      //   //redirecting to signin screen after success
      // }, error => {
      //   console.log(error);// Error getting the data

      //   let alert = this.alertCtrl.create({
      //     title: 'Signup Response',
      //     subTitle: data.toString(),
      //     buttons: ['OK']
      //   });
      //   alert.present();
      //   loader.dismiss();
        
      // });
  }
  //  setData()
  //  {
  //     this.storage.set('title', this.title);
  //     this.storage.set('start_time', this.start_time);
  //     this.storage.set('end_time', this.end_time);
  //     this.storage.set('number',this.number);
  //     this.storage.set('description',this.description); 
  //     this.storage.set('plan_id',this.target_plan_id);
  //     this.storage.set('plan_id',this.curr_dep_amount);         
  //   }                      

//     getData(){
// // Getting email, password and address from storage
//     this.storage.get('name').then((val) => {
//       console.log('Name is:', val);
//     });

//       this.storage.get('email').then((val) => {
//       console.log('Email is:', val);
//     });

//     this.storage.get('password').then((val) => {
//       console.log('Password is:', val);
//     });

//     this.storage.get('country').then((val) => {
//       console.log('country is:', val);
//     });
//     }
 doAlert() {
  let toast = this.toastCtrl.create({
      message: 'Target Created Successfully.',
      duration: 2000
    });
    toast.present();
  }

}


