import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { LecturePage } from "../lecture/lecture";
import { HomePage } from "../home/home";

@Component({
  selector: 'page-terms',
  templateUrl: 'terms.html'
})
export class termsPage {

  constructor(public navCtrl: NavController, private menuCtrl: MenuController) {

  }
  log()
  {
    this.navCtrl.push(HomePage);
  }
  OnOpenMenu()
  {
    this.menuCtrl.open();
  }

}
