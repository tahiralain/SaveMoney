import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';

@Component({
  selector: 'page-downloaded',
  templateUrl: 'downloaded.html'
})
export class DownloadedPage {

  constructor(public navCtrl: NavController, private menuCtrl: MenuController) {

  }
OnOpenMenu()
  {
    this.menuCtrl.open();
  }
 myFunction() {
    var x = document.getElementById('myDIV');
    if (x.style.display === 'none') {
        x.style.display = 'block';
    } else {
        x.style.display = 'none';
    }
}
  
  showhide() {       
    var div = document.getElementById("newpost");
    if (div.style.display !== "none") {
        div.style.display = "none";
    }
    else {
        div.style.display = "block";
    }
     }
}
