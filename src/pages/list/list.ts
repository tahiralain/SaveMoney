import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { RecieptPage } from "../reciept/reciept";
import { Http } from '@angular/http';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
 
 daily: string;
  curr_dep_amount: string;
  target_plan_id: string;
  description: string;
  posts: any;
  title: '';
  start_time: '';
  end_time: '';
  number: '';
  model = [];
  
  data:any;
  private apiUrl='https://btckeycard.com/savemoney/user_transaction.php';
  constructor(public navCtrl: NavController, public menuCtrl: MenuController ,private http: Http) {
    // If we navigated to this page, we will have an item available as a nav param
    this.http.get(this.apiUrl).map(res => res.json()).subscribe(data => {
    console.log(data);
    this.posts = data;
   // console.log(this.posts);
   
    
      }, error => {
         console.log(error);// Error getting the data
      });

//console.log(this.model);
  

  }
  OnOpenMenu()
  {
    this.menuCtrl.open();
  }
  rec(){
    this.navCtrl.push(RecieptPage);
  }
}
