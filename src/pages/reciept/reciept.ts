import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';

@Component({
  selector: 'page-reciept',
  templateUrl: 'reciept.html',
})
export class RecieptPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController) {
  }

OnOpenMenu()
  {
    this.menuCtrl.open();
  }
}
